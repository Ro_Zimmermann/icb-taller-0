### Imports ###
import random
from itertools import combinations

### Funciones Auxiliares ###

def valoresPosta(lista):                                # en algún momento vamos a necesitar usar los valores 8, 9 y 10 para los 10, 11 y 12 respectivamente   
    i = 0
    valores_posta = []
    while i < len(lista):
        if lista[i] < 10:
            valores_posta.append(lista[i])
        elif lista[i] == 10:
            valores_posta.append(8)
        elif lista[i] == 11:
            valores_posta.append(9)
        elif lista[i] == 12:
            valores_posta.append(10)
        i = i + 1
    return valores_posta

# mazo_prueba = [5, 12, 10, 11, 2]
# mazo_posta = valoresPosta(mazo_prueba)
# print('Mazo de prueba: ', mazo_prueba)
# print('Mazo con valores posta: ', mazo_posta)         # probamos la fc. Comparamos mazo_prueba con mazo_posta y vemos que 10, 11 y 12 pasan a ser 8, 9 y 10 al implementar la fc

def valorNominal(lista):                                # de la misma forma, en algún momento vamos a necesitar ver las cartas 10, 11 y 12 en vez de los valores 8, 9 y 10 respectivamente
    i = 0
    valores_nominales = []
    while i < len(lista):
        if lista[i] < 8:
            valores_nominales.append(lista[i])
        elif lista[i] == 8:
            valores_nominales.append(10)
        elif lista[i] == 9:
            valores_nominales.append(11)
        elif lista[i] == 10:
            valores_nominales.append(12)
        i = i + 1
    return valores_nominales

# mazo_valores_nominales = valorNominal(mazo_posta)
# print('Mazo con los valores nominales de las cartas: ', mazo_valores_nominales) # probamos la fc. Comparamos mazo_posta con mazo_valores_nominales y vemos que hace el cambio deseado. Asimismo, mazo_valores_nominales es igual a mazo_prueba

def valorNominalEntero(integer):                        # más adelante me encuentro con que necesito lo mismo, pero que acepte como argumento un entero. Así que por poco óptimo que sea, creo esta fc
    if integer < 8:
        integer = integer
    elif integer == 8:
        integer = 10
    elif integer == 9:
        integer = 11
    elif integer == 10:
        integer = 12
    return integer

# valor_nominal = valorNominalEntero(2)                 # pruebo la fc. Varío el valor que le paso como argumento a la fc (2, 8, 9, 10) y me fijo que me devuelva lo que espero (2, 10, 11, 12)
# print(valor_nominal)

def generarMazo():
    palo = []
    i = 1
    while i <= 10:                                      # cada palo tiene 10 cartas. Como va del 1 al 10, este mazo tiene los valores posta de las cartas
        palo.append(i)
        i = i + 1
    mazo = palo*4                                       # un mazo tiene 4 palos
    random.shuffle(mazo)                                # mezclamos
    mazo_nominal = valorNominal(mazo)                   # hace que el mazo tenga los valores nominales de las cartas
    return mazo_nominal

# mazo = generarMazo()                                  # probamos la fc
# print(len(mazo))                                      # tiene la cantidad correcta de cartas
# print('Mazo: ', mazo)

## Otras opciones: mazo = [x for x in range(1, 13) if x =! 8 and x=! 9] *4
            
def repartir(mazo, jug1, jug2, jug3, jug4):
    i = 0
    if len(mazo) >= 12:                                     # si el mazo tiene suficientes cartas, le da 3 cartas a cada uno
        while i < 3:
            jug1.append(mazo.pop(0))                        # saca una del mazo y la agrega al jugador
            jug2.append(mazo.pop(0))
            jug3.append(mazo.pop(0))
            jug4.append(mazo.pop(0))
            i = i + 1
    return mazo                                         # los jugadores quedan con las cartas, la función devuelve el mazo modificado

# jugador_1 = []                                        # generamos jugadores para probar la fc
# jugador_2 = []
# jugador_3 = []
# jugador_4 = []
# repartida = repartir(mazo, jugador_1, jugador_2, jugador_3, jugador_4)  # probamos la fc
# print(len(mazo))                                                        # queda con la cantidad correcta de cartas
# print('Nuevo mazo: ', repartida)
# print('Jugador 1: ', jugador_1, '\nJugador 2: ', jugador_2, '\nJugador 3: ', jugador_3, '\nJugador 4: ', jugador_4)

## Otras opciones: hacer una lista con los jugadores y que el while recorra la lista appendeando. Se podría hacer una función aux darCarta que sea la parte .append(mazo.pop(0)) para que quede más claro. Así:


def iniciarMesa(mazo, mesa):
    i = 0
    while len(mazo) >= 4 and i < 4:     # si el mazo tiene suficientes cartas, pone 4 cartas sobre la mesa
        mesa.append(mazo.pop(0))        # saca una del mazo y la pone en la mesa
        i = i + 1
    return                              # la mesa queda con las cartas, el mazo con menos cartas

# mesa = []                             # generamos la mesa para probar la fc
# iniciarMesa(mazo, mesa)               # probamos la fc
# print(len(mazo))                      # tiene la cantidad correcta de cartas
# print('Nuevo mazo: ', mazo)
# print('Mesa: ', mesa)

## Otras opciones: la condición de que el mazo tenga al menos 4 cartas (el requiere en la especificación) se puede dejar de lado si asumimos que el mazo viene siempre con 4 cartas o más (porque después de generarlo, repartí y entonces llamo esta fc)
## La fc puede no tener return!! Pero en ese caso, tengo que ver bien lo que hace la fc (fijarme si no necesito que me devuelva algo).

def suma(a):
    suma = 0
    i = 0
    while i < len(a):
        suma = suma + a[i]              # va sumando elemento a elemento de la lista
        i = i + 1
    return suma

# lista = [2, 2, 3, 4]
# print(suma(lista))                    # probamos la fc

def subListas(lista):                               # genera todas las sublistas de una lista dada. Propuesta en la consigna
    resultado = []
    for k in range (1, len(lista) + 1):
        for listita in combinations(lista, k):      # para usar combinations tuve que importarlo de itertools, más arriba
            resultado.append(list(listita))
    return resultado

# print(subListas([1, 2, 3]))                       # chequeo veloz para ver que funciona 

def juegosPosibles(jug, mesa):
    jug = valoresPosta(jug)                                         # hacemos que jugador y mesa tengan los valores posta de las cartas
    mesa = valoresPosta(mesa)
    subListas_mesa = subListas(mesa)                                # generamos todas las combinaciones posibles de las cartas de la mesa
    posibles_juegos = []
    i = 0
    while i < len(jug):                                             # recorre las cartas del jugador
        j = 0
        while j < len(subListas_mesa):                              # recorre las combinaciones de cartas de la mesa
            if jug[i] + suma(subListas_mesa[j]) == 15:              # si la carta del jugador y la combinación de cartas de la mesa suman 15
                carta_jugador = valorNominalEntero(jug[i])
                posibles_juegos.append(carta_jugador)               # entonces agrega el juego (transformado de nuevo al valor nominal) a la lista de posibles juegos
                cartas_mesa = valorNominal(subListas_mesa[j])
                posibles_juegos.append(cartas_mesa)
            j = j + 1
        i = i + 1
    return posibles_juegos

# posibles_juegos_jug_1 = juegosPosibles(jugador_1, mesa)           # probamos la fc: arma todos los posibles juegos del jugador indicado, y suman 15
# posibles_juegos_jug_2 = juegosPosibles(jugador_2, mesa)
# posibles_juegos_jug_3 = juegosPosibles(jugador_3, mesa)
# posibles_juegos_jug_4 = juegosPosibles(jugador_4, mesa)
# print('Posibles jugadas jugador 1: ', posibles_juegos_jug_1)
# print('Posibles jugadas jugador 2: ', posibles_juegos_jug_2)
# print('Posibles jugadas jugador 3: ', posibles_juegos_jug_3)
# print('Posibles jugadas jugador 4: ', posibles_juegos_jug_4)


def elegirMejor(juegos):
    cantidad_cartas_mesa = 0                                    # cuenta la cantidad de cartas de la mesa que se lleva cada juego posible
    mejor = []
    i = 1                                                       # el jugador usa siempre una carta de su mano y levanta cartas de la mesa (índices impares de la lista), alcanza con comparar cuantas cartas levantaría de la mesa con cada juego. El juego en el que levante más cartas será el mejor
    while i < len(juegos):
        if len(juegos[i]) >= cantidad_cartas_mesa:              # si la cantidad de cartas que levanta de la mesa es la misma o más que la cantidad de cartas de otros posibles juegos
            cantidad_cartas_mesa = len(juegos[i])               # entonces actualiza la variable que tiene la cantidad de cartas que se levantan de la mesa en el mejor juego hasta el momento
            mejor = [juegos[i-1], juegos[i]]                    # y guarda el mejor juego hasta ahora (la carta del jugador y las cartas de la mesa)
        i = i + 2                                               # salta al siguiente índice impar
    return mejor

# mejor_juego_jug_1 = elegirMejor(posibles_juegos_jug_1)        # probamos la fc. Con lista vacía devuelve lista vacía (check)
# mejor_juego_jug_2 = elegirMejor(posibles_juegos_jug_2)
# mejor_juego_jug_3 = elegirMejor(posibles_juegos_jug_3)
# mejor_juego_jug_4 = elegirMejor(posibles_juegos_jug_4)
# print('Mejor jugada jugador 1: ', mejor_juego_jug_1)
# print('Mejor jugada jugador 2: ', mejor_juego_jug_2)
# print('Mejor jugada jugador 3: ', mejor_juego_jug_3)
# print('Mejor jugada jugador 4: ', mejor_juego_jug_4)

def jugar(mesa, jug, basa):
    if len(jug) > 0:                                                # si el jugador tiene cartas
        juegos_posibles = juegosPosibles(jug, mesa)                 # arma todos los juegos posibles
        mejor_juego = elegirMejor(juegos_posibles)                  # elige uno
        
        if len(mejor_juego) != 0:                                   # si hay un juego posible, lo realiza
            a_basa = []
            i = 0
            while i < len(jug):
                if mejor_juego[0] == jug[i]:                        # busca en la mano la carta del juego y la agrega a la basa
                    a_basa.append(jug.pop(i))
                i = i + 1
            i = 0
            while i < len(mejor_juego[1]):
                j = 0
                while j < len(mesa):
                    if mejor_juego[1][i] == mesa[j]:                # busca en la mesa la(s) carta(s) del juego y la(s) agrega a la basa
                        a_basa.append(mesa.pop(j))
                    j = j + 1
                i = i + 1
            basa.append(a_basa)                                     # pone el juego en la basa del jugador
        else:                                                       # si no hay un juego posible, baja una carta a la mesa
            mesa.append(jug.pop(0))
        
    return                                                          # modifica las listas que recibe, no devuelve nada

## El return en este caso puede tanto estar como no. ATENCION al hacer .pop, modifica los índices de la lista! --> Tener en cuenta, corregir con el if/else ekl avance del contador j=j // j=j+1.
## Además, si encuentra dos cartas iguales en la mesa, las saca las dos! Hay que cortar el loop antes. Idem en las cartas del jugador.  

# basa_jug_1 = []                                                   # generamos las basas para probar la fc
# basa_jug_2 = []
# basa_jug_3 = []
# basa_jug_4 = []
# jugar(mesa, jugador_1, basa_jug_1)                                # probamos la fc
# jugar(mesa, jugador_2, basa_jug_2)
# jugar(mesa, jugador_3, basa_jug_3)
# jugar(mesa, jugador_4, basa_jug_4)
# print('Mesa: ', mesa)
# print('Jugador 1: ', jugador_1, '\nJugador 2: ', jugador_2, '\nJugador 3: ', jugador_3, '\nJugador 4: ', jugador_4)                             # todos los jugadores bajaron una carta
# print('Basa jugador 1: ', basa_jug_1, '\nBasa jugador 2: ', basa_jug_2, '\nBasa jugador 3: ', basa_jug_3, '\nBasa jugador 4: ', basa_jug_4)     # los juegos se agregaron a las basas

def jugarRonda(mesa, jug1, basa1, jug2, basa2, jug3, basa3, jug4, basa4):
    while len(jug1) > 0 or len(jug2) > 0 or len(jug3) > 0 or len(jug4) > 0:     # mientras alguno de los jugadores tenga cartas, hace una ronda
        jugar(mesa, jug1, basa1)
        jugar(mesa, jug2, basa2)
        jugar(mesa, jug3, basa3)
        jugar(mesa, jug4, basa4)
    return                                                                      # modifica las listas que recibe, no devuelve nada

# ronda = jugarRonda(mesa, jugador_1, basa_jug_1, jugador_2, basa_jug_2, jugador_3, basa_jug_3, jugador_4, basa_jug_4)
# print('Mesa: ', mesa)
# print('Jugador 1: ', jugador_1, '\nJugador 2: ', jugador_2, '\nJugador 3: ', jugador_3, '\nJugador 4: ', jugador_4)                             # todos los jugadores bajaron todas las cartas
# print('Basa jugador 1: ', basa_jug_1, '\nBasa jugador 2: ', basa_jug_2, '\nBasa jugador 3: ', basa_jug_3, '\nBasa jugador 4: ', basa_jug_4)     # los juegos se agregaron a las basas

def sumaPuntos(basa1, basa2, basa3, basa4):
    i = 0
    cartas1 = 0
    while i < len(basa1):                                       # recorre la basa del jugador 1
        cartas1 = cartas1 + len(basa1[i])                       # agrega la cantidad de cartas de cada jugada al contador cartas
        i = i + 1
    
    i = 0
    cartas2 = 0
    while i < len(basa2):                                       # recorre la basa del jugador 2
        cartas2 = cartas2 + len(basa2[i])                       # agrega la cantidad de cartas de cada jugada al contador cartas
        i = i + 1
    
    i = 0
    cartas3 = 0
    while i < len(basa3):                                       # recorre la basa del jugador 3
        cartas3 = cartas3 + len(basa3[i])                       # agrega la cantidad de cartas de cada jugada al contador cartas
        i = i + 1
    
    i = 0
    cartas4 = 0
    while i < len(basa4):                                       # recorre la basa del jugador 4
        cartas4 = cartas4 + len(basa4[i])                       # agrega la cantidad de cartas de cada jugada al contador cartas
        i = i + 1
    
    if cartas1 > cartas2 and cartas1 > cartas3 and cartas1 > cartas4:             # si el jugador 1 tiene más cartas, obtiene un punto
        puntos = [1, 0, 0, 0]
    elif cartas2 > cartas1 and cartas2 > cartas3 and cartas2 > cartas4:           # si el jugador 2 tiene más cartas, obtiene un punto
        puntos = [0, 1, 0, 0]
    elif cartas3 > cartas1 and cartas3 > cartas2 and cartas3 > cartas4:           # si el jugador 3 tiene más cartas, obtiene un punto
        puntos = [0, 0, 1, 0]
    elif cartas4 > cartas1 and cartas4 > cartas2 and cartas4 > cartas3:           # si el jugador 4 tiene más cartas, obtiene un punto
        puntos = [0, 0, 0, 1]
    else:                                                                         # si empatan, nadie tiene puntos
        puntos = [0, 0, 0, 0]
    
    return puntos

# puntos = sumaPuntos(basa_jug_1, basa_jug_2, basa_jug_3, basa_jug_4)             # probamos la fc
# print('Puntos: ', puntos)

## Otra forma: con fc beta
## b(True) = 1
## b(False) = 0
## O buscar el índice del máximo valor de la lista con argmax

def quienGano(basa1, basa2, basa3, basa4):
    puntos = sumaPuntos(basa1, basa2, basa3, basa4)    
    
    i = 0
    ganador = 'Hubo un empate'                                          # si ningun jugador obtuvo puntos, hubo un empate. Es el default. 
    while i < len(puntos):                                              # recorre la lista puntos
        if puntos[i] == 1:                                              # al jugador con mas cartas (es decir el que sumo un punto), lo nombra ganador
            ganador = 'Ganó el jugador ' + str(i+1)                     # str convierte el argumento a tipo string
        i = i + 1
    return ganador

# ganador = quienGano(basa_jug_1, basa_jug_2, basa_jug_3, basa_jug_4)   # probamos la fc
# print('- Quién ganó?\n+', ganador)

### Programa Principal ###

jugador_1 = []                      # generamos los jugadores
jugador_2 = []
jugador_3 = []
jugador_4 = []

basa_jug_1 = []                     # generamos las basas
basa_jug_2 = []
basa_jug_3 = []
basa_jug_4 = []

mazo = generarMazo()                # generamos el mazo

mesa = []                           # generamos la mesa
iniciarMesa(mazo, mesa)

while len(mazo) > 0:                # mientras haya cartas en el mazo, sigue repartiendo y haciendo rondas de juego. En realidad, esto solo funciona para cuando hay al menos 12 cartas en el mazo (por la condición que impone la fc repartir), y funciona bien cuando el mazo arranca con 40 cartas
    repartir(mazo, jugador_1, jugador_2, jugador_3, jugador_4)                                                      # repartimos las cartas
    jugarRonda(mesa, jugador_1, basa_jug_1, jugador_2, basa_jug_2, jugador_3, basa_jug_3, jugador_4, basa_jug_4)

ganador = quienGano(basa_jug_1, basa_jug_2, basa_jug_3, basa_jug_4)  # vemos quien gana

print('Basa jugador 1: ', basa_jug_1, '\nBasa jugador 2: ', basa_jug_2, '\nBasa jugador 3: ', basa_jug_3, '\nBasa jugador 4: ', basa_jug_4)
print('- Quién ganó?\n+', ganador) 

### Ejercicio Optativo ###
# Una idea podría ser generar un mazo, repartir y guardar dentro de variables prima los juegos iniciales de los jugadores, la mesa y el mazo iniciales.
# Luego, habría que utilizar las últimas líneas del programa principal varias veces, usando las variables prima, cambiando el orden de los jugadores al ingresarlos a jugarRonda en el while, y notando quién sale ganador cada vez.
# Para esto último podrían crearse una o dos funciones auxiliares que hagan random la posición que toma cada jugador en el juego, que iteren este proceso y vayan guardando los resultados (posición de cada jugador y ganador, por ej) de cada iteración, y que 
# busquen/comparen si hay una posición en la que un jugador resulta ganador más veces, o si hay un jugador que independientemente de su posición gana más veces. 
# Idealmente, habría que repetir este proceso (generar mazo, repartir, guardar en variables prima, jugar variando el orden de los jugadores y ver quién gana) varias veces para tener significancia estadística.
# Si siempre tiende a ganar el mismo jugador sin importar la posición en la que esté, la posición en la ronda no influye en la probabilidad de ganar, si no más bien la identidad de las cartas que se le repartieron en principio, las de la mesa y los otros jugadores. 
# Pero si tiende a ganar más veces el jugador que está en la posición i, independientemente de qué jugador sea, la posición en la ronda sí influye en la posibilidad de ganar. 
# Todo esto sería válido únicamente para las reglas de juego que consideramos en este programa. 