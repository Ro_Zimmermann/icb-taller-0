## Mini Taller 1

## Decimos que una lista es "triangular" si es creciente hasta cierto elemento, y a partir de ese
## elemento es decreciente. Por ejemplo, la lista [2; 4; 5; 7; 4; 3] es triangular, mientras que la lista
## [2; 4; 5; 7; 5; 8; 4; 3; 1] no lo es. La lista vacia es triangular y la lista solo creciente (o decreciente)
## tambien lo es.
## Implementar una funcion llamada esTriangular, que tome como parametro una lista de
## enteros, y que determine si la lista es triangular o no.

## Funciones Auxiliares

def sublista_creciente(lista):                              # Fc que dice si la primera parte de una lista (entre el primer elemento y el elemento máximo) es creciente o no. 

    if len(lista) != 0:                                     # Si el largo de la lista es distinto de 0, procede a ver si es creciente o no. Si es 0, por default es creciente. 
        indice_maximo = lista.index(max(lista))             # Indica cuál es el indice maximo de la lista
        lista_inf = lista[0 : indice_maximo + 1]            # Crea una sublista que va desde el primer elemento hasta el máximo
        
        for i in range(len(lista_inf) - 1):                 # Recorre la sublista hasta el anteúltimo elemento (que luego compara con el último, si no se iría de rango)
            if lista_inf[i] > lista_inf[i+1]:
                return False                                # Si no es creciente, devuelve False
  
    return True                                             # Si es creciente, devuelve True

# lista = [1, 2, 3, 1]                                                                                  # Casos de prueba
# lista_1 = [5, 2, 3, 1, 4]
# lista_2 = [2, 3, 1, 4, 5]
# lista_3 = []
# lista_4 = [1]
# lista_5 = [1, 2]

# creciente_lista = sublista_creciente(lista)
# creciente_lista_1 = sublista_creciente(lista_1)
# creciente_lista_2 = sublista_creciente(lista_2)
# creciente_lista_3 = sublista_creciente(lista_3)
# creciente_lista_4 = sublista_creciente(lista_4)
# creciente_lista_5 = sublista_creciente(lista_5)

# print("La sublista inferior de la lista ", lista, " es creciente: ", creciente_lista)                  # Todos dan lo esperado. La lista vacía y las de 1 y 2 elementos son crecientes. 
# print("La sublista inferior de la lista ", lista_1, " es creciente: ", creciente_lista_1)
# print("La sublista inferior de la lista ", lista_2, " es creciente: ", creciente_lista_2)
# print("La sublista inferior de la lista ", lista_3, " es creciente: ", creciente_lista_3)
# print("La sublista inferior de la lista ", lista_4, " es creciente: ", creciente_lista_4)
# print("La sublista inferior de la lista ", lista_5, " es creciente: ", creciente_lista_5)


def sublista_decreciente(lista):                            # Fc que dice si la segunda parte de una lista (entre el elemento máximo y el último elemento) es decreciente o no. 
    
    if len(lista) != 0:                                     # Si el largo de la lista es distinto de 0, procede a ver si es decreciente o no. Si es 0, por default es decreciente. 
        indice_maximo = lista.index(max(lista))             # Indica cuál es el indice maximo de la lista
        lista_sup = lista[indice_maximo : len(lista)]       # Crea una sublista que va desde el elemento máximo hasta el último elemento. 
        
        for i in range(len(lista_sup) - 1):                 # Recorre la sublista hasta el anteúltimo elemento (que luego compara con el último, si no se iría de rango)
            if lista_sup[i] < lista_sup[i+1]:
                return False                                # Si no es decreciente, devuelve False
               
    return True                                             # Si es decreciente, devuelve True

# lista = [1, 2, 3, 2, 1]                                                                               # Casos de prueba
# lista_1 = [5, 2, 3, 1, 4]
# lista_2 = [2, 3, 1, 4, 5]
# lista_3 = []
# lista_4 = [1]
# lista_5 = [1, 2]

# decreciente_lista = sublista_decreciente(lista)
# decreciente_lista_1 = sublista_decreciente(lista_1)
# decreciente_lista_2 = sublista_decreciente(lista_2)
# decreciente_lista_3 = sublista_decreciente(lista_3)
# decreciente_lista_4 = sublista_decreciente(lista_4)
# decreciente_lista_5 = sublista_decreciente(lista_5)

# print("La sublista superior de la lista ", lista, " es decreciente: ", decreciente_lista)              # Todos dan lo esperado. La lista vacía y las de 1 y 2 elementos son decrecientes. 
# print("La sublista superior de la lista ", lista_1, " es decreciente: ", decreciente_lista_1)
# print("La sublista superior de la lista ", lista_2, " es decreciente: ", decreciente_lista_2)
# print("La sublista superior de la lista ", lista_3, " es decreciente: ", decreciente_lista_3)
# print("La sublista superior de la lista ", lista_4, " es decreciente: ", decreciente_lista_4)
# print("La sublista superior de la lista ", lista_5, " es decreciente: ", decreciente_lista_5)


## Funciones Principales

def esTriangular(lista):
    esCreciente = sublista_creciente(lista)                           # Aplica las fc aux y ve si la lista tiene sublistas creciente y decreciente
    esDecreciente = sublista_decreciente(lista)
    
    if esCreciente == True and esDecreciente == True:                 # Si tiene ambas sublistas, la lista es triangular (True)
        return True
    else:                                                             # Si no las tiene, no es triangular (False)
        return False

# lista = [2, 4, 5, 7, 4, 3]                                                     # Casos de prueba
# lista_1 = [2, 4, 5, 7, 5, 8, 4, 3, 1]
# lista_2 = [5, 3, 2, 1]
# lista_3 = [1, 2, 3, 5]
# lista_4 = []
# lista_5 = [1]
# lista_6 = [1, 2]

# triangular_lista = esTriangular(lista)
# triangular_lista_1 = esTriangular(lista_1)
# triangular_lista_2 = esTriangular(lista_2)
# triangular_lista_3 = esTriangular(lista_3)
# triangular_lista_4 = esTriangular(lista_4)
# triangular_lista_5 = esTriangular(lista_5)
# triangular_lista_6 = esTriangular(lista_6)

# print("La lista ", lista, " es triangular: ", triangular_lista)                # Todos dan lo esperado. La lista vacía y las de 1 y 2 elementos son triangulares. 
# print("La lista ", lista_1, " es triangular: ", triangular_lista_1)
# print("La lista ", lista_2, " es triangular: ", triangular_lista_2)
# print("La lista ", lista_3, " es triangular: ", triangular_lista_3)
# print("La lista ", lista_4, " es triangular: ", triangular_lista_4)
# print("La lista ", lista_5, " es triangular: ", triangular_lista_5)
# print("La lista ", lista_6, " es triangular: ", triangular_lista_6)

## La implementación además cumple con la especificación (archivo anexo en la carpeta).

## GUARDA las funciones externas que llamo también pueden estar iterando sobre las listas. Los nombres es_creciente y es_decreciente de las funciones auxiliares eran confusos, no del todo declarativos. 
## FALTA ver que pasa con las mesetas en el máximo y unificar el criterio respecto a ellas.