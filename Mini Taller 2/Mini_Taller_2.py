## Mini Taller 2

## 1) Especificar e implementar una funcion llamada cantSub que recibe dos cadenas de caracteres y devuelve la cantidad de veces que aparece la segunda en la primera.
## Por ejemplo, si recibimos la cadena "Vienen los zombies!, no quiero que un zombie se coma mi brazo, porque sino me volvere un zombie que come otro brazo", 
## la cadena "zombie" aparece tres veces.

## Funciones Auxiliares

def encontrePalabra(palabra, frase, i):                                        # Compara la frase desde la posición i con la palabra que yo estoy buscando
    j = 0
    while j < len(palabra):                                                    # Itera por la palabra
        if palabra[j] != frase[i + j]:                                         # Si el elemento de la palabra y el elemento de la frase (desde la posición indicada) no coinciden, no es la palabra que busca. 
            return False
        if palabra[j] == frase[i + j]:                                         # Si el elemento de la palabra y el elemento de la frase desde la posición indicada coinciden en cada iteración, es la palabra que busca. 
            j = j + 1
    return True

# palabra = "Hola"                                                             # Casos de prueba
# frase = "Hola que tal"
# encontrada = encontrePalabra(palabra, frase, 0)                              # Probando con i = 0 da True, con i = 6 da False. Con situaciones triviales (ninguna palabra o alguna palabra coincide) da lo esperado. 
# print(encontrada)

## Comentarios: la igualdad al comparar los elementos en esta función auxiliar es estricta (diferencia mayúsculas y minúsculas).
## Esta función se rompe cuando la frase es una string vacía pero la palabra no. Sin embargo, esta función va a estar en el contexto de la función principal, 
## que maneja de otra forma los casos de frase vacía. Cuando la palabra es una string vacía pero la frase no, siempre da True; sin embargo este caso queda excluido 
## por el requiere de la especificación. 

## Función Principal

def cantSub(frase, palabra):
    i = 0
    contador = 0
    while i < len(frase) - len(palabra):
        if encontrePalabra(palabra, frase, i):
            contador = contador + 1
        i = i + 1
    return contador
            
# palabra = "Hola"                                                             # Casos de prueba
# frase = "Hola Hola hola que tal"
# encontradas = cantSub(frase, palabra)                                           
# print(encontradas)

# cuantos_zombies = cantSub("Vienen los zombies!, no quiero que un zombie se coma mi brazo, porque sino me volvere un zombie que come otro brazo", "zombie")
# print(cuantos_zombies)

## Nuevamente, la comparación entre elementos es estricta (distingue mayúsculas de minúsculas). Probando con una string vacía en frase, devuelve 0 (OK). Cuando la palabra es una string vacía, encuentra tantas palabras como elementos tiene la string frase (este caso no nos incumbe
## porque se descarta en el requiere). 


## 2) Especificar e implementar la funcion llamada unirProlijo que recibe dos listas de numeros ordenadas y devuelve otra lista que corresponde con la union ordenada de las recibidas.
## Por ejemplo, si se reciben las listas [1.2, 1.4, 1.6, 1.8] y [1.1, 1.5, 1.6, 2.0], deberia devolver [1.1, 1.2, 1.4, 1.5, 1.6, 1.6, 1.8, 2.0].
    
## Función Principal

def unirProlijo(lista1, lista2):                                               # Une dos listas ordenadas en una única lista ordenada
    lista = []
    
    while len(lista1) != 0 and len(lista2) != 0:
        if lista1[0] < lista2[0]:                                              # Si el primer elemento de la lista1 es menor, lo saca y lo agrega a la lista
            lista.append(lista1.pop(0))
        elif lista1[0] > lista2[0]:                                            # Si el primer elemento de la lista2 es menor, lo saca y lo agrega a la lista
            lista.append(lista2.pop(0))
        elif lista1[0] == lista2[0]:                                           # Si ambos elementos son iguales, saca el primero de ambas listas y los agrega a la lista
            lista.append(lista1.pop(0))
            lista.append(lista2.pop(0))
    
    if len(lista1) == 0:                                                       # Si la lista1 es vacía, le agrega a la lista lo que queda de la lista2
        lista.extend(lista2)                                                   # Lista.extend() nos permite agregar a una lista los elementos de otra pero como elementos separados. 
    elif len(lista2) == 0:                                                     # Si la lista2 es vacía, le agrega a la lista lo que queda de la lista1
        lista.extend(lista1)
                
    return lista

# lista1 = [1.2, 1.4, 1.6, 1.8]                                                # Casos de prueba
# lista2 = [1.1, 1.5, 1.6, 2.0]
# union = unirProlijo(lista1, lista2)
# print(union)                                                                 # Con listas de diferentes longitudes, una de las dos listas vacías, o las dos listas vacías da lo esperado. Listas con elementos repetidos también devuelven lo esperado. 