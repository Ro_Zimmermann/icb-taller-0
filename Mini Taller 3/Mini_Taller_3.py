### Mini Taller 3

## Imports
import numpy as np
import timeit
import matplotlib.pyplot as plt

## Las funciones deben recibir una lista de enteros y modificarla para ordenarla en forma ascendente. 

## 1) upSortSlice que implementa la version del algoritmo de busqueda utilizando slices cada vez que se debe achicar la lista.

## Función Auxiliar

def maxPos(lista, inicial, actual):
    max_valor = lista[inicial]                                                 # Toma el primer valor (e índice) de la sublista por default
    indice_max = inicial
    
    while inicial < actual:                                                    # Mientras inicial sea menor que actual
        if lista[actual] >= max_valor:                                         # Compara el valor del elemento actual con el valor del elemento inicial/el valor del elemento máximo encontrado hasta el momento. Si es mayor o igual, lo pone como nuevo valor/índice máximo. 
            max_valor = lista[actual]
            indice_max = actual
        actual -= 1
    
    return indice_max

# lista = [3, 2, 1, 4]                                                         # Casos de prueba. Con la lista [4] da 0, con lista vacía da 0. Con elementos negativos también funciona según lo esperado, por ej con la lista [-3, -2, 0, -4] devuelve 2.
# prueba = maxPos(lista, 0, len(lista)-1)                                      # Cuando actual es menor que len(lista)-1, por ejemplo 2, y la lista es [3, 2, 1, 4] (con el máximo elemento fuera del rango indicado), devuelve el índice esperado (0)
# print(prueba)

## Para que maxPos funcione, requiere que inicial y actual estén dentro de los índices de la lista, osea que inicial sea <= 0 y que actual sea <= len(lista)-1 (sigue la especificación dada en teórica).
## Además, se rompe con listas vacías. Esto se  trata poniendo una condición previa en la función de ordenamiento en la que se llame maxPos. 

## Función Principal

def upSortSlice(lista):
    if not lista:                                                              # Si la lista es vacía, retorna lista vacía. Esto evita el único caso que rompe a maxPos devolviendo el resultado esperado. 
        return []
    else:                                                                      # Si la lista no es vacía
        ordenada = []
        while lista:                                                           # Mientras la lista no sea vacía 
            actual = len(lista)-1    
            maximo = maxPos(lista, 0, actual)                                  # Busca el mayor elemento entre la posición 0 y actual y devuelve el índice, lo asigna. 
            lista[maximo], lista[actual] = lista[actual], lista[maximo]        # Swap del elemento máximo con el elemento de la posición actual. 
            ordenada = [lista[actual]] + ordenada                              # Agrega el elemento máximo encontrado al inicio de la lista ordenada. 
            lista = lista[:-1]                                                 # Saca el elemento ya ordenado (el último) de la lista. 
        return ordenada

# lista = [4, 3, 5, 1]                                                         # Casos de prueba: [4, 3, 5, 1] -> [1, 3, 4, 5]; [1, 2, 3, 4] -> [1, 2, 3, 4]; [] -> []; [2] -> [2]; [4, 4, 5, 6] -> [4, 4, 5, 6]; [4, 4, 5, 6, 1] -> [1, 4, 4, 5, 6]. Funciona como se espera.  
# prueba = upSortSlice(lista)
# print(prueba)


## 2) upSortIndex que implementa la misma version del algoritmo, pero utiliza swaps para intercambiar los elementos y una variable para indicar el limite del segmento de busqueda.

def upSortIndex(lista):
    if not lista:                                                              # Si la lista es vacía, retorna lista vacía. Esto evita el único caso que rompe a maxPos devolviendo el resultado esperado. 
        return []
    else:
        actual = len(lista) - 1                                                # Se define actual como la última posición de la lista, antes de entrar al ciclo. 
        while actual > 0:                                                      # Mientras actual sea mayor a 0, itera sobre la lista de atrás para adelante. La guarda podría ser también actual >= 0, en cuyo caso compararía el elemento de la posición 0 dos veces, porque se le indica abajo en maxPos que tome el elemento 0 como inicial. 
            maximo = maxPos(lista, 0, actual)                                  # Busca el mayor elemento entre la posición 0 y actual y devuelve el índice, lo asigna. 
            lista[maximo], lista[actual] = lista[actual], lista[maximo]        # Swap del elemento máximo con el elemento de la posición actual. 
            actual -= 1
        return lista

# lista = [4, 3, 5, 1]                                                         # Casos de prueba: [4, 3, 5, 1] -> [1, 3, 4, 5]; [1, 2, 3, 4] -> [1, 2, 3, 4]; [] -> []; [2] -> [2]; [4, 4, 5, 6] -> [4, 4, 5, 6]; [4, 4, 5, 6, 1] -> [1, 4, 4, 5, 6]. Funciona como se espera.  
# prueba = upSortIndex(lista)
# print(prueba)


## 3) bubbleSort, en este caso solo habra una version del algoritmo ya que se usaran swaps para intercambiar los valores.

def bubbleSort(lista):
    for i in range(0, len(lista)):                                             # Recorre todos los elementos de la lista. 
        for j in range(0, len(lista)-1):                                       # Recorre todos los elementos de la lista hasta el anteúltimo, que compara con el último elemento. 
            if lista[j] > lista[j + 1]:                                        # Si el elemento de la posición en la que está es mayor que el elemento de la siguiente, los swapea. 
                lista[j], lista[j + 1] = lista[j + 1], lista[j]
    return lista

# lista = [4, 3, 5, 1]                                                         # Casos de prueba: [4, 3, 5, 1] -> [1, 3, 4, 5]; [1, 2, 3, 4] -> [1, 2, 3, 4]; [] -> []; [2] -> [2]; [4, 4, 5, 6] -> [4, 4, 5, 6]; [4, 4, 5, 6, 1] -> [1, 4, 4, 5, 6]. Funciona como se espera.  
# prueba = bubbleSort(lista)
# print(prueba)


## 4) mergeSortSlices debe ordenar la lista por medio del algoritmo recursivo de referencia obteniendo slices de las sublistas para hacer la llamada recursiva.

## Función Auxiliar (la dejo acá en vez de ponerla al principio para facilitar la lectura)

def conquer_n_combine(lista, inferior, superior):

    i = 0                                                                      # Iterador para la sublista inferior
    j = 0                                                                      # Iterador para la sublista superior
    k = 0                                                                      # Iterador para la lista original
        
    while i < len(inferior) and j < len(superior):                             # Si ninguno de los iteradores fue avanzado
        if inferior[i] <= superior[j]:                                         # Elijo el menor
            lista[k] = inferior[i]                                             # Lo pongo en la posición k de la lista original, avanzando el iterador que le corresponde
            i += 1
        else:
            lista[k] = superior[j]
            j += 1

        k += 1                                                                 # Avanzo el iterador k de la lista original a la siguiente posición
            
    while i < len(inferior):                                                   # Si el que agregué a la lista original fue el elemento de la sublista superior, el iterador de la inferior aún no fue avanzado y el elemento de la sublista inferior es mayor al de la sublista superior
        lista[k] = inferior[i]                                                 # Lo agrego a la lista original y avanzo los contadores
        i += 1
        k += 1
    
    while j < len(superior):                                                   # Si el que agregué a la lista original fue el elemento de la sublista inferior, el iterador de la superior aún no fue avanzado y el elemento de la sublista superior es mayor al de la sublista inferior
        lista[k]=superior[j]                                                   # Lo agrego a la lista original y avanzo los contadores
        j += 1
        k += 1

## Función Principal

def mergeSortSlices(lista):

    if len(lista) > 1:                                                         # Si la lista tiene más de un elemento
    
        medio = len(lista) // 2                                                # Índice medio
        inferior = lista[:medio]                                               # Se queda la mitad inferior de la lista
        superior = lista[medio:]                                               # Se queda la mitad superior de la lista
    
        mergeSortSlices(inferior)                                              # Llamadas recursivas de la fc sobre ambas sublistas
        mergeSortSlices(superior)
        conquer_n_combine(lista, inferior, superior)
        
    return lista

# lista = [4, 3, 5, 1]                                                         # Casos de prueba: [4, 3, 5, 1] -> [1, 3, 4, 5]; [1, 2, 3, 4] -> [1, 2, 3, 4]; [] -> []; [2] -> [2]; [4, 4, 5, 6] -> [4, 4, 5, 6]; [4, 4, 5, 6, 1] -> [1, 4, 4, 5, 6]. Funciona como se espera.  
# prueba = mergeSortSlices(lista)
# print(prueba)

## 5) mergeSortIndex es similar al anterior, solo que los limites de los segmentos de busqueda seran marcados por variables.

## Función Auxiliar (la dejo acá en vez de ponerla al principio para facilitar la lectura)

def conquer_n_combine_index(lista, inicio, medio, final):
        
    inferior = lista[inicio:medio]                                             # Crea las sublistas inferior y superior
    superior = lista[medio:(final + 1)]
        
    i = 0                                                                      # Iterador para la sublista inferior
    j = 0                                                                      # Iterador para la sublista superior
    k = inicio                                                                 # Iterador para la lista original. Arranca desde la posición inicio
        
    while i < len(inferior) and j < len(superior):                             # Si ninguno de los iteradores fue avanzado
        if inferior[i] <= superior[j]:                                         # Elijo el menor
            lista[k] = inferior[i]                                             # Lo pongo en la posición k de la lista original, avanzando el iterador que le corresponde
            i += 1
        else:
            lista[k] = superior[j]
            j += 1

        k += 1                                                                 # Avanzo el iterador k de la lista original a la siguiente posición
            
    while i < len(inferior):                                                   # Si el que agregué a la lista original fue el elemento de la sublista superior, el iterador de la inferior aún no fue avanzado y el elemento de la sublista inferior es mayor al de la sublista superior
        lista[k] = inferior[i]                                                 # Lo agrego a la lista original y avanzo los contadores
        i += 1
        k += 1
    
    while j < len(superior):                                                   # Si el que agregué a la lista original fue el elemento de la sublista inferior, el iterador de la superior aún no fue avanzado y el elemento de la sublista superior es mayor al de la sublista inferior
        lista[k] = superior[j]                                                 # Lo agrego a la lista original y avanzo los contadores
        j += 1
        k += 1

## Función Principal

def mergeSortIndex(lista, inicio, final):

    if final - inicio == 1:                                                    # Caso base: la lista tiene dos elementos
        if lista[inicio] > lista[final]:                                       # Si están desordenados, los ordeno con un swap 
            lista[inicio], lista[final] = lista[final], lista[inicio]
                
    if final - inicio >= 2:                                                    # Si la lista tiene más de dos elementos
    
        medio = (inicio + final) // 2                                          # Índice medio
    
        mergeSortIndex(lista, inicio, medio - 1)                               # Llamadas recursivas de la fc sobre la mitad inferior y la superior de la lista. La mitad superior incluye el índice medio. 
        mergeSortIndex(lista, medio, final)
        conquer_n_combine_index(lista, inicio, medio, final)


# lista = [4, 3, 5, 1]                                                         # Casos de prueba: [4, 3, 5, 1] -> [1, 3, 4, 5]; [1, 2, 3, 4] -> [1, 2, 3, 4]; [] -> []; [2] -> [2]; [4, 4, 5, 6] -> [4, 4, 5, 6]; [4, 4, 5, 6, 1] -> [1, 4, 4, 5, 6]. Funciona como se espera.  
# mergeSortIndex(lista, 0, len(lista) - 1)
# print(lista)


## 6) quickSortCopy implementa el algoritmo de ordenamiento copiando los elementos a nuevas sublistas en la etapa de particionado (se puede usar concatenacion para unirlas).

## Función Auxiliar (la dejo acá en vez de ponerla al principio para facilitar la lectura)

def particionar_copia(lista):
    menores = []                                                               # Crea listas vacías para agregar los elementos según corresponda
    pivot = [lista[0]]                                                         # Elige el primer elemento de la lista como pivot
    mayores = [] 
    
    for i in range(1, len(lista)):                                             # Recorre los elementos de la lista salvo el primero, que es el pivot
        if lista[i] < pivot[0]:                                                # Si el elemento es menor al pivot, lo agrega a menores
            menores.append(lista[i])
        else:                                                                  # Si el elemento es mayor o igual al pivot, lo agrega a mayores
            mayores.append(lista[i])            
    return menores, pivot, mayores

# lista = [3, 2, 1, 4]                                                         # Casos de prueba. Con la lista [4] da ([], [4], []), con lista vacía se rompe.
# prueba = particionar_copia(lista)                                      
# print(prueba)

## particionar_copia se rompe con lista vacía, con listas de un elemento funciona. Sin embargo, ambos casos son tratados en quickSortCopy antes de llegar a llamar la función auxiliar. 

## Función Principal

def quickSortCopy(lista):
    
    if len(lista) < 2:                                                         # Caso base. Si la lista es vacía o sólo tiene un elemento
        return lista                                                           # Ya está ordenada
    
    menores, pivot, mayores = particionar_copia(lista)                         # Particiona la lista
        
    return quickSortCopy(menores) + pivot + quickSortCopy(mayores)             # Caso recursivo. Ordena las sublistas menor y mayor usando quickSortCopy, y el resultado es la concatenación ordenada de las listas ordenadas
    
# lista = [4, 3, 5, 1]                                                         # Casos de prueba: [4, 3, 5, 1] -> [1, 3, 4, 5]; [1, 2, 3, 4] -> [1, 2, 3, 4]; [] -> []; [2] -> [2]; [4, 4, 5, 6] -> [4, 4, 5, 6]; [4, 4, 5, 6, 1] -> [1, 4, 4, 5, 6]. Funciona como se espera.  
# prueba = quickSortCopy(lista)
# print(prueba)


## 7) quickSortIndex que implementa el mismo algoritmo del punto anterior, pero haciendo swap sobre la misma lista.

## Función Auxiliar (la dejo acá en vez de ponerla al principio para facilitar la lectura)

def particionar_swap(lista, inicio, final):                                    # Indica la posición donde se va a realizar la partición (la posición del pivot), una vez hecho el swap de los elementos para acomodarlos según menores-pivot-mayores

  pivot = lista[final]                                                         # Elige el último elemento de la lista como pivot
  i = inicio - 1                                                               # Señala el elemento mayor

  for j in range(inicio, final):                                               # Recorre todos los elementos en la porción indicada de la lista, excepto el último que es el pivot
    if lista[j] <= pivot:                                                      # Si el elemento en la posición j es menor que el pivot
      i += 1

      (lista[i], lista[j]) = (lista[j], lista[i])                              # Hace el swap de los elementos de i y j

  (lista[i + 1], lista[final]) = (lista[final], lista[i + 1])                  # Hace el swap entre el último elemento de la lista (pivot) y el elemento más grande dado por i
  return i + 1                                                                 # Retorna la posición donde se hace la partición, que es donde queda el pivot después del swap

# lista = [3, 2, 1, 4]                                                         # Casos de prueba. Con la lista [4] da 0, con lista vacía se rompe.
# prueba = particionar_swap(lista, 0, len(lista) - 1)                                      
# print(prueba)

## particionar_swap se rompe con lista vacía, con listas de un elemento funciona. Sin embargo, ambos casos son tratados en quickSortIndex antes de llegar a llamar la función auxiliar. 

## Función Principal

def quickSortIndex(lista, inicio, final):
  if inicio < final:                                                           # Caso recursivo.  

    pivot = particionar_swap(lista, inicio, final)                             # Al pasar por la fc particionar_swap, el índice devuelto es el índice del pivot, con los números menores a él a la izquierda y los mayores a la derecha.

    quickSortIndex(lista, inicio, pivot - 1)                                   # Llamadas recursivas sobre las sublistas izquierda y derecha
    quickSortIndex(lista, pivot + 1, final)                                    # Esta función no retorna nada, modifica directamente la lista que se le pasó como argumento
    
# lista = [4, 3, 5, 1]                                                         # Casos de prueba: [4, 3, 5, 1] -> [1, 3, 4, 5]; [1, 2, 3, 4] -> [1, 2, 3, 4]; [] -> []; [2] -> [2]; [4, 4, 5, 6] -> [4, 4, 5, 6]; [4, 4, 5, 6, 1] -> [1, 4, 4, 5, 6]. Funciona como se espera.  
# quickSortIndex(lista, 0, len(lista) - 1)
# print("Lista ordenada: ", lista)



### Desempeño de los algoritmos

def crear_listas():
    for i in range(1, 7):
        lista = np.random.randint(1, 1000, (6, 10*10**i))                      # Crea un array con 6 listas que tienen entre 100 y 10.10^6 elementos en incrementos de 10.10^(i+1) con i entre 1 y 6. Los elementos son valores random entre 1 y 1000. 
    return lista

def registrar_tiempo(funcion, lista):                                          # Devuelve el resultado del tiempo de cómputo de la función sin índices con la lista dada
    t = timeit.timeit(lambda: funcion(lista))
    return t

def registrar_tiempo_index(funcion, lista, inicio, final):  # Devuelve el resultado del tiempo de cómputo de la función con índices con la lista dada
    t = timeit.timeit(lambda: funcion(lista, inicio, final))
    return t

## Notar que al registrar el tiempo la lista queda ordenada. 

# lista = [4, 3, 5, 1]                                                         # Pruebas

# print(registrar_tiempo(bubbleSort, lista))
# print(lista)

# print(registrar_tiempo_index(quickSortIndex, lista)) 
# print(lista)   


def registrar_tiempos_algoritmos():                                            # Devuelve un array con los tiempos de cómputo de todos los algoritmos (columnas) con todas las listas (filas)
    listas = crear_listas()                                                    # Crea las listas que se usarán de prueba
    tiempos = np.zeros((6, 8))                                                 # El array vacío para los tiempos
    
    for i in range(0, 6):                                                      # Itera las listas de prueba
        lista = list(listas[i])
        tiempos[i, 0] = timeit.timeit(lambda: lista.sort())                    # Toma el tiempo del algoritmo con esa lista y lo guarda en la posición correspondiente del array tiempos
    
    for i in range(0, 6):
        lista = list(listas[i])
        tiempos[i, 1] = registrar_tiempo(upSortSlice, lista)
        
    for i in range(0, 6):
        lista = list(listas[i])
        tiempos[i, 2] = registrar_tiempo_index(upSortIndex, lista, 0, len(lista)-1)
    
    for i in range(0, 6):
        lista = list(listas[i])
        tiempos[i, 3] = registrar_tiempo(bubbleSort, lista)

    for i in range(0, 6):
        lista = list(listas[i])
        tiempos[i, 4] = registrar_tiempo(mergeSortSlices, lista)

    for i in range(0, 6):
        lista = list(listas[i])
        tiempos[i, 5] = registrar_tiempo_index(mergeSortIndex, lista, 0, len(lista)-1)
        
    for i in range(0, 6):
        lista = list(listas[i])
        tiempos[i, 6] = registrar_tiempo(quickSortCopy, lista)
    
    for i in range(0, 6):
        lista = list(listas[i])
        tiempos[i, 7] = registrar_tiempo_index(quickSortIndex, lista, 0, len(lista)-1)
    
    return tiempos



## Datos que se van a graficar

# tiempos = registrar_tiempos_algoritmos()                                     # Va a tardar bocha en correr. La pruebo a lo último
tiempos = np.random.randint(1, 10, (6,8))                                      # De juguete, para hacer el gráfico

## El array tiempos tiene la forma tiempos[fila (listas), columna (algoritmos)]. Es como una tabla de doble entrada, donde las filas tienen las listas de diferentes longitudes, las columnas cada algoritmo y cada celda el tiempo de cómputo de ese algoritmo con esa lista

x = np.array([100, 1000, 10000, 100000, 1000000, 1000000])                     # En el eje x van los tamaños de las listas

## Gráficos 

fig = plt.figure()

plt.plot(x, tiempos[:,0], label = "sort (referencia)")                         # Plotea el tiempo que tardó en correr en función del tamaño de la lista, para cada algoritmo. 
plt.plot(x, tiempos[:,1], label = "upSortSlice")
plt.plot(x, tiempos[:,2], label = "upSortIndex")
plt.plot(x, tiempos[:,3], label = "bubbleSort")
plt.plot(x, tiempos[:,4], label = "mergeSortSlices")
plt.plot(x, tiempos[:,5], label = "mergeSortIndex")
plt.plot(x, tiempos[:,6], label = "quickSortCopy")
plt.plot(x, tiempos[:,7], label = "quickSortIndex")

plt.title("Tiempo de cómputo de los algoritmos en función del tamaño de entrada")
plt.xlabel("Tamaño de la lista")
plt.ylabel("Tiempo de cómputo (s)")
plt.legend(loc = 'best')

plt.show()
plt.savefig("C:/Users/rosar/Desktop/Tiempos de cómputo.pdf")

## No es el mejor gráfico que hice en mi vida. No sé por qué no lo guarda como pdf. 
