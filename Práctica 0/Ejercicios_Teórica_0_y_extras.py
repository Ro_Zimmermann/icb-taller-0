# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 12:52:38 2021

@author: rosar
"""
# Ejercicios teórica 0

# 1
# Defina una función que tome dos cadenas de caracteres como parámetro y devuelva la de mayor longitud. Complete el siguiente
# programa:
def masLarga(l1, l2):
    if len(l1)>len(l2):
        return l1
    else:
        return l2

nombre1 = 'Pepe'
nombre2 = 'Chirizo'
print(masLarga(nombre1, nombre2))

# 2
# Defina una función que recibe una lista y devuelve la cantidad de letras e que contiene. 
def cantE(l):
    i = 0
    count = 0
    while i < len(l):
        if l[i] == 'e':
            count = count + 1
        i = i + 1
    return count

muchas_e = 'tengo muchas eeee'
print(cantE(muchas_e))

# 3
# Dada la siguiente definición de una función:
def esPar(a):
    if a %2 == 0:
        return True
    return False
# Implementar una función que tome una lista y cambie todos sus elementos pares por la letra 'P'. Debe utilizar la función
# esPar definida arriba. 

def cambia_pares_por_P(l):
    for i in range(len(l)):
        if esPar(l[i]) == True:
            l[i] = 'P'
    return l

lista = [1, 2, 3, 4, 5, 6]
print(cambia_pares_por_P(lista))

# 4
# Defina una función que tome una lista y cambie todas las vocales por -.
def cambia_vocal(string):
    lista = list(string)
    for i in range(len(lista)):
        if lista[i] == 'a' or lista[i] == 'e' or lista[i] == 'i' or lista[i] == 'o' or lista[i] == 'u':
            lista[i] = '-'
    string = str(lista)
    return string

lista = 'Be silent. Keep your forked tongue behind your teeth.'
print(cambia_vocal(lista))

# 5
# Defina la función mezclar que tome dos listas y devuelva una lista que sea el resultado de intercalar elemento a elemento. 
# Por ejemplo: si intercalamos Pepe con Jose daría PJeopsee. 

def mezclar(l1, l2):
    mezcla = []
    i = 0       ## Usamos dos contadores cuando queremos recorrer las listas de dos formas distintas. 
    while i < len(l1) or i < len(l2):
        if i < len(l1) and i < len(l2):
            mezcla.append(l1[i])
            mezcla.append(l2[i])
        elif i < len(l1) and i >= len(l2):
            mezcla.append(l1[i])
        else:
            mezcla.append(l2[i])
        i = i + 1
    return mezcla


lista1 = ['P', 'e', 'p', 'e']
lista2 = ['J', 'o', 's', 'e', 'c', 'i', 't', 'o']
print(mezclar(lista1, lista2))


# Ejercicio extra mail

# Una mañana ponés un billete en la vereda al lado del obelisco porteño. A partir de ahí, cada día vas y duplicás la cantidad 
# de billetes, apilándolos prolijamente. ¿Cuánto tiempo pasa antes de que la pila de billetes sea más alta que el obelisco?
# Datos: grosor de cada billete (en metros) y altura del obelisco (en metros)
# grosor_billete = 0.11 * 0.001
# altura_obelisco = 67.5 

def tiempo_hasta_h_obelisco():
    dia = 1
    altura = 0.11 * 0.001   # Altura de un billete
    while altura < 67.5:    # 67.5 es la altura del obelisco
        dia = dia + 1
        altura = altura*2
    return print(dia - 1, altura)

tiempo_hasta_h_obelisco()

# También:
def tiempo_hasta_h_obelisco():
    dia = 0     # Considero al día que puse el billete como día 0
    altura = 0.11 * 0.001   # Altura de un billete
    while altura < 67.5:    # 67.5 es la altura del obelisco
        dia = dia + 1
        altura = altura*2
    print(dia - 1, altura)
    # return print(dia - 1, altura) NO return print() porque así la función sólo me imprime lo que le pido, no me devuelve ningún valor. 
    return dia - 1, altura

tiempo_hasta_h_obelisco()