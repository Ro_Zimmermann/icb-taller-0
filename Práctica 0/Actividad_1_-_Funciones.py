# -*- coding: utf-8 -*-
"""
Created on Mon Sep  6 00:10:20 2021

@author: rosar
"""

# Actividad I: Funciones
# Completar y probar las siguientes funciones:

# 1
def devolver_numero_5():
    numero = 5
    return numero

print(devolver_numero_5())

# 2
def devolver_una_lista():
    lista = [5, 6, 7, 8, 9, 10]
    return lista

print(devolver_una_lista())

# 3
def devolver_misma_lista(una_lista):
    return una_lista

print(devolver_misma_lista([1, 2, 3]))

# 4
def devolver_la_suma(numero1, numero2):
    suma = numero1 + numero2
    return suma

print(devolver_la_suma(5,6))


# Últimos ejercicios

# 1. Definir una función que sea la multiplicación de dos números. 
def multiplicación(numero1, numero2):
    multiplicacion = numero1*numero2
    return multiplicacion

print(multiplicación(5, 5))

# 2. Definir una función que reciba tres coeficientes correspondientes al polinomio a*x^2 + b*x + c y devuelva el
# valor de evaluarlo en el cuarto valor a recibir (correspondiente a x en la expresión anterior).
def polinomio(a, b, c, x):
    y = a*x*x + b*x + c
    return y

print(polinomio(1, 1, 1, 2))