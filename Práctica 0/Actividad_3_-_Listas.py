## Actividad 3 - Listas

# 1. sumaDivisores(n): devuelve la suma de todos los divisores positivos de n.
def sumaDivisores(n): 
    suma_divisores_positivos = 0
    for i in range(1,n+1):
      if n % i == 0:   # si el resto es 0, es divisor
          suma_divisores_positivos = suma_divisores_positivos + i   # entonces lo agrego a la suma
    return suma_divisores_positivos

print(sumaDivisores(5))

# 2. suma(a): devuelve la suma de todos los elementos de la lista a.
def suma(a):
    suma = 0
    for i in range(len(a)):
        suma = suma + a[i]   # va sumando elemento a elemento de la lista
    return suma

lista = [2, 2, 3, 4]
print(suma(lista))

# 3. listaDeAbs(a): devuelve una lista con los valores absolutos de cada elemento de la lista a.
def listaDeAbs(a):
    lista_abs = []   # nueva lista para los valores absolutos
    for i in range(len(a)):
        lista_abs.append(abs(a[i]))   # le agrega el valor absoluto a la lista
    return lista_abs

lista = [-50, 70, -10]
lista_vacia = []
print(listaDeAbs(lista))
print(listaDeAbs(lista_vacia))

# Con while y *********sin abs******* ver
def listaDeAbs(a):
    lista_abs = []   # nueva lista para los valores absolutos
    i = 0
    while i < len(a):
        lista_abs.append(abs(a[i]))   # le agrega el valor absoluto a la lista
        i = i + 1
    return lista_abs

lista = [-50, 70, -10]
lista_vacia = []
print(listaDeAbs(lista))
print(listaDeAbs(lista_vacia))

# 4. masRepetido(a): devuelve el elemento que mas veces aparece repetido en la lista a.
def masRepetido(a):
    for i in range(len(a)):
        mas_repetidos = []
        if a.count(a[i]) > a.count(a[i+1]):   # elemento i se repite más veces
            mas_repetido = a[i]
        elif a.count(a[i]) < a.count(a[i+1]):   # elemento i+1 se repite más veces
            mas_repetido = a[i+1]
        elif a.count(a[i]) == a.count(a[i+1]) and a[i] != a[i+1]:   # se repiten la misma cantidad de veces y son distintos
            mas_repetidos.append(a[i])
        else:   # se repiten la misma cantidad de veces y son iguales
            mas_repetido = a[i]
            
        for j in range(len(mas_repetidos)):
            if mas_repetido == mas_repetidos[j]:   # el resultado del más repetido está entre la lista de más repetidos, omitimos
                return mas_repetido
            else:   # el resultado del más repetido no está entre la lista de más repetidos, lo agregamos
                return mas_repetidos.append(mas_repetido)

lista = ['h', 'h', 'h', 'h', 'o', 'l', 'a', 'h', 'o', 'l', 'a', 's']
lista2 = ['h', 'h', 'h', "q", 'u', 'u', 'u']
print(masRepetido(lista))
    
## MODULARIZAR CÓDIGO
    
    
    # repeticiones = []   # cuantas veces se repite cada elemento
    # for i in range(len(a)):
    #     repeticiones.append(a.count(a[i]))   # contamos cuantas veces se repite el elemento i y lo anexamos a repeticiones
    
    # for i in range(len(repeticiones)):   # buscamos el que más se repite
    #     if repeticiones[i] > repeticiones[i+1]:   # elemento i se repite más
    #         mas_repetido = repeticiones[i]
    #     else:
    #         mas_repetido = repeticiones[i+1]   # elemento i + 1 se repite más 
    