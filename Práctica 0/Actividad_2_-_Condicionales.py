# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 21:28:32 2021

@author: rosar
"""

# Actividad II: Condicionales
# Completar y probar las siguientes funciones:

# 1
def decir_si_existe_un_numero_mas_grande_que_5(n):
    numero = n
    return numero > 5

numero = 9
print(decir_si_existe_un_numero_mas_grande_que_5(numero))

# 2
def decir_si_la_longitud_es_mayor_a_5(l):
    return l > 5

longitud = 20
print(decir_si_la_longitud_es_mayor_a_5(longitud))

# 3
def decir_si_la_longitud_es_igual_a(un_nombre, un_numero):
    return len(un_nombre) == un_numero

un_nombre = 'Pepe'
un_numero = 5
print(decir_si_la_longitud_es_igual_a(un_nombre, un_numero))

# 4
def devolver_valor_mas_grande(valor1, valor2):
    if valor1 > valor2:
        resultado = valor1
    else:
        resultado = valor2
    return resultado

valor1 = 5
valor2 = 6
print(devolver_valor_mas_grande(valor1, valor2))

# También puede hacerse en menos pasos:
def devolver_valor_mas_grande(valor1, valor2):
     if valor1 > valor2:
          return valor1
     else:
          return valor2

# O también:
def devolver_valor_mas_grande(valor1, valor2):
     if valor1 > valor2:
          return valor1
     return valor2


# Condiciones combinadas

def es_par(un_numero):
    resto = un_numero % 2
    return resto == 0

# Completar y probar las siguientes funciones:

# 1
def devolver_el_doble_si_es_par(un_numero):
    if es_par(un_numero):
        resultado = un_numero*2
    else:
        resultado = un_numero
    return resultado

un_numero = 71
print(devolver_el_doble_si_es_par(un_numero))

# 2
def devolver_valor_si_es_par_sino_el_que_sigue(un_numero):
    if es_par(un_numero):
        resultado = un_numero
    else:
        resultado = un_numero + 1
    return resultado

un_numero = 71
print(devolver_valor_si_es_par_sino_el_que_sigue(un_numero))

# 3
def devolver_segun_condiciones_locas(un_numero):
    if un_numero == 2:
        resultado = un_numero + 1
    elif un_numero <= 10:
        resultado = un_numero*2
    elif un_numero > 20 and un_numero < 34:
        resultado = un_numero + 5
    else:
        resultado = 0
    return resultado

un_numero = 100
print(devolver_segun_condiciones_locas(un_numero))

# Más ejercicios para practicar
# Definir e implementar las siguientes funciones. En todos los casos se recomienda escribirlas y probarlas con distintos 
# ejemplos para verificar su funcionamiento.

# 1. Definir una función que recibe un número y devuelve el doble si es más grande que 10 y menor a 20. 
def devolver_el_doble_si_esta_entre_10_y_20(un_numero):
    if un_numero > 10 and un_numero < 20:
        return un_numero*2      ## Si el if es correcto, llega al return y no sigue adelante. 

un_numero = 15
print(devolver_el_doble_si_esta_entre_10_y_20(un_numero))

# 2. Definir una función que recibe un número y devuelve "Rayos y Centellas" si es más grande que 20 o menor a 5.
def devolver_rayos_y_centellas(un_numero):
    if un_numero > 20 or un_numero < 5:
        return 'Rayos y Centellas'

un_numero = 3
print(devolver_rayos_y_centellas(un_numero))

# 3. Definir una función que recibe un número y devuelve "Está en el rango deseado" si el valor está entre 5 y 10 y 
# "Fuera de rango" en caso contrario. 
def devolver_dentro_o_fuera_del_rango(un_numero):
    if un_numero > 5 and un_numero < 10:
        return 'Está en el rango deseado'
    else:
        return 'Fuera de rango'

un_numero = 8
print(devolver_dentro_o_fuera_del_rango(un_numero))

# 4. Definir una función que recibe un número y devuelve "Menor a 5" si el valor es menor a 5, "Entre 10 y 20"
# si el valor está entre 10 y 20, y en cualquier otro caso que devuelva "Número muy grande". 
def clasificar_numero(un_numero):
    if un_numero < 5:
        return 'Menor a 5'
    elif un_numero > 10 and un_numero < 20:
        return 'Entre 10 y 20'
    else:
        return 'Número muy grande'

un_numero = 0
print(clasificar_numero(un_numero))

# O también:
def clasificar_numero(un_numero):
    if un_numero < 5:
        return "Menor a 5"
    if un_numero < 20 and numero > 10:
        return "Entre 10 y 20"
    return "Número muy grande"